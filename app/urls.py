# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

from app.api.urls import router as api_router


urlpatterns = [
    url(r'^api/v1/', include(api_router.urls)),
]
