# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.core.wsgi import get_wsgi_application

from app.databases import Database


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings.dev')

application = get_wsgi_application()
db = Database()
