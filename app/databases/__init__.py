# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.databases.mongo import Mongo


class Database(object):

    def __init__(self):
        super(Database, self).__init__()
        # add connections to different dbs here
        self.mongo = Mongo()
