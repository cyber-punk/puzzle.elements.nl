# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mongoengine.connection import register_connection
from django.conf import settings


class Mongo(object):

    def __init__(self):
        super(Mongo, self).__init__()
        self.register_connections()

    @staticmethod
    def register_connections():
        for alias, params in settings.MONGODB_DATABASES.items():
            register_connection(alias, **params)
