# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, utils

from app.common.models import AbstractModel
from app.common.serializers.exc import ValidationError


class DataMixin(Schema):

    def load_data(self, *args, **kwargs):
        result = self.load(*args, **kwargs)
        if result.errors:
            raise ValidationError(result.errors)
        return result.data

    def dump_data(self, *args, **kwargs):
        result = self.dump(*args, **kwargs)
        if result.errors:
            raise ValidationError(result.errors)
        return result.data


class CollectionsDumperMixin(Schema):

    @staticmethod
    def _is_many(obj):
        return utils.is_collection(obj) and not isinstance(obj, AbstractModel)

    def dump(self, obj, **kwargs):
        kwargs['many'] = self._is_many(obj)
        return super(CollectionsDumperMixin, self).dump(obj, **kwargs)
