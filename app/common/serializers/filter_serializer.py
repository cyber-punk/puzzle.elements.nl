# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from copy import deepcopy

from marshmallow import Schema

from app.common.serializers.abstract_serializer import AbstractSerializer
from app.common.serializers.mixins import DataMixin, CollectionsDumperMixin


class FilterSerializer(DataMixin, CollectionsDumperMixin, Schema):

    def __init__(self, serializer, *args, **kwargs):
        super(FilterSerializer, self).__init__(*args, **kwargs)

        assert isinstance(serializer, AbstractSerializer)
        self.serializer = serializer

        self.declared_fields = deepcopy(self.serializer.declared_fields)
        self.fields = deepcopy(self.serializer.fields)

    def load(self, data, **kwargs):
        kwargs['many'] = isinstance(data, list)
        kwargs['partial'] = True
        return super(FilterSerializer, self).load(data, **kwargs)
