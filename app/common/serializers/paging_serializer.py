# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, fields, post_load
from marshmallow.validate import Range

from app.common.serializers.mixins import DataMixin
from app.common.models import SerializerModel


class PagingSerializer(DataMixin, Schema):
    offset = fields.Integer(default=0, validate=Range(min=0))
    limit = fields.Integer(default=1024, validate=Range(min=1, max=1024))
    count = fields.Integer(dump_only=True)
    total = fields.Integer(dump_only=True)

    def load(self, data, **kwargs):
        kwargs['many'] = False
        kwargs['partial'] = False
        return super(PagingSerializer, self).load(data, **kwargs)

    @post_load(pass_many=False)
    def make_object(self, data):
        return SerializerModel(self, data)
