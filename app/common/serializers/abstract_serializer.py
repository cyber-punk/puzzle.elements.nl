# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from marshmallow import Schema, fields, post_load, validates_schema
from marshmallow.exceptions import ValidationError

from app.common.serializers.mixins import DataMixin, CollectionsDumperMixin
from app.common.models import SerializerModel


class AbstractSerializer(DataMixin, CollectionsDumperMixin, Schema):
    id = fields.UUID(default=uuid.uuid4)

    def load(self, data, **kwargs):
        partial = kwargs.get('partial')
        if partial is not None:
            self.partial = partial
        kwargs['many'] = isinstance(data, list)
        return super(AbstractSerializer, self).load(data, **kwargs)

    @post_load
    def make_object(self, data):
        return SerializerModel(self, data)

    @validates_schema
    def validate_data_on_create(self, data):
        if (not self.partial and
                not self.fields['id'].required and 'id' in data):
            raise ValidationError('Should be empty.', field_names=['id'])

    @validates_schema
    def validate_data_on_update(self, data):
        if self.partial and data.get('id') is None:
            raise ValidationError('Can not be empty.', field_names=['id'])
