# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class ValidationError(Exception):

    def __init__(self, errors):
        super(ValidationError, self).__init__()
        self.errors = errors
