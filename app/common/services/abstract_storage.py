# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.models import AbstractModel


class AbstractStorage(object):
    model_cls = AbstractModel

    def __init__(self):
        super(AbstractStorage, self).__init__()
        self.model_cls = self.__class__.model_cls
        assert issubclass(self.model_cls, AbstractModel)

    def count(self, **attrs):
        return self._get_count(**attrs)

    def read(self, paging=None, **attrs):
        if 'id' in attrs:
            return self._get_item(**attrs)
        else:
            return self._get_list(paging=paging, **attrs)

    def _get_item(self, **attrs):
        raise NotImplementedError('_get_item method is not implemented')

    def _get_list(self, paging=None, **attrs):
        raise NotImplementedError('_get_list method is not implemented')

    def _get_count(self, **attrs):
        raise NotImplementedError('_get_count method is not implemented')

    def create(self, **attrs):
        model = self.model_cls(**attrs)
        self._save(model)
        return model

    def update(self, **attrs):
        model = self._get_item(id=attrs.get('id'))
        for attr_name, attr_value in attrs.items():
            if hasattr(model, attr_name):
                setattr(model, attr_name, attr_value)
        self._save(model)
        return model

    def _save(self, model, **kwargs):
        raise NotImplementedError('_save method is not implemented')

    def delete(self, **attrs):
        self._remove(**attrs)

    def _remove(self, **attrs):
        raise NotImplementedError('_remove method is not implemented')
