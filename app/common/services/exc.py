# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class StorageException(Exception):
    description = 'Storage error'


class ReadOnlyError(StorageException, IOError):
    description = 'Storage is readonly'


class ResourceException(StorageException):
    description = 'Resource {} error'

    def __init__(self, *args, **attrs):
        super(ResourceException, self).__init__(*args)
        items = []
        for attr_name, attr_value in attrs.items():
            setattr(self, attr_name, attr_value)
            items.append('{}={}'.format(attr_name, attr_value))
        self.description = self.__class__.description.format(', '.join(items))


class DoesNotExist(ResourceException):
    description = 'Resource {} does not exist'


class AlreadyExists(ResourceException):
    description = 'Resource {} already exists'
