# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import csv

from app.common.services.abstract_storage import AbstractStorage
from app.common.services.exc import ReadOnlyError


class CSVStorage(AbstractStorage):

    def __init__(self, iterable, dialect='excel',
                 delimiter=',', quotechar='"',
                 logger=None):
        super(CSVStorage, self).__init__()
        self.iterable = iterable
        self.dialect = dialect
        self.delimiter = delimiter.encode('utf-8')
        self.quotechar = quotechar.encode('utf-8')
        self.logger = logger or logging.getLogger(__name__)

    @staticmethod
    def _preprocess_column(name, value):  # pylint: disable=unused-argument
        if value == '':
            return None
        return value.replace('\n', ' ').strip()

    def _iter_items(self, paging=None, **attrs):
        reader = csv.reader(self.iterable,
                            dialect=self.dialect,
                            delimiter=self.delimiter,
                            quotechar=self.quotechar)
        row_number = -1
        columns = None
        for row in reader:
            self.logger.debug(row)

            if columns is None:
                # The first row can contain column names, ignore it.
                # In other case uses ordered model attributes list.
                if set(row).intersection(self.model_cls.attr_names):
                    columns = row
                    continue
                else:
                    columns = self.model_cls.attr_names

            row_number += 1
            if paging:
                if row_number <= paging.offset:
                    continue
                if row_number > paging.offset + paging.limit:
                    break

            # row length can be difference with fields count
            data = {}
            for i in range(min(len(columns), len(row))):
                data[columns[i]] = self._preprocess_column(
                    name=columns[i],
                    value=row[i]
                )
            is_valid = not attrs or all([
                data.get(attr_name) == attr_value
                for attr_name, attr_value in attrs.items()
            ])
            if not is_valid:
                continue
            yield self.model_cls(**data)

    def _get_count(self, **attrs):
        return len(list(self._iter_items(paging=None, **attrs)))

    def _get_list(self, paging=None, **attrs):
        return list(self._iter_items(paging=paging, **attrs))

    def _get_item(self, **attrs):
        return None  # not supported

    def _save(self, model, **kwargs):
        raise ReadOnlyError

    def _remove(self, **attrs):
        raise ReadOnlyError
