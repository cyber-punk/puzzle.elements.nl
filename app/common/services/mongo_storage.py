# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mongoengine import queryset

from app.common.services.abstract_storage import AbstractStorage
from app.common.services.exc import DoesNotExist, AlreadyExists
from app.common.models import MongoModel


class MongoStorage(AbstractStorage):
    model_cls = MongoModel

    def __init__(self):
        super(MongoStorage, self).__init__()
        assert issubclass(self.model_cls, MongoModel)

    def _get_query(self, **attrs):
        return self.model_cls.objects(**attrs)

    def _get_item(self, **attrs):
        result = self._get_query(**attrs).first()
        if not result:
            raise DoesNotExist(**attrs)
        return result

    def _get_list(self, paging=None, **attrs):
        query = self._get_query(**attrs)
        if paging is not None:
            query = query.limit(paging.limit)
            query = query.skip(paging.offset)
        return query.all()

    def _get_count(self, **attrs):
        query = self._get_query(**attrs)
        return query.count()

    def create(self, **attrs):
        model = self.model_cls(**attrs)
        self._save(model, force_insert=True)
        return model

    def _save(self, model, force_insert=False, **kwargs):
        try:
            model.save(force_insert=force_insert)
        except queryset.NotUniqueError as e:
            raise AlreadyExists(*e.args)

    def _remove(self, **attrs):
        try:
            self._get_query(**attrs).delete()
        except queryset.DoesNotExist as e:
            raise DoesNotExist(*e.args, **attrs)
