# -*- coding: utf-8 -*-
# pylint: disable=abstract-method
# pylint: disable=protected-access
from __future__ import unicode_literals

import unittest
from mock import patch, MagicMock

from app.common.utils import patch_multiple
from app.common.models import AbstractModel
from app.common.services import AbstractStorage


AUTO_INC_ID = 0


class MyModel(AbstractModel):
    my_field = None

    def __init__(self, **attrs):
        super(MyModel, self).__init__(**attrs)
        if self.id is None:
            global AUTO_INC_ID  # pylint: disable=global-statement
            AUTO_INC_ID += 1
            self.id = AUTO_INC_ID


class MyStorage(AbstractStorage):
    model_cls = MyModel


TEST_STR_1 = 'That life should be valued at less than a stocking'
TEST_STR_2 = 'And breaking of frames lead to breaking of bones'


class Test(unittest.TestCase):
    """
    Test AbstractStorage in isolation mode
    """

    def setUp(self):
        global AUTO_INC_ID  # pylint: disable=global-statement
        AUTO_INC_ID = 0

    def test_has_attr_create(self):
        self.assertTrue(hasattr(AbstractStorage, 'create'))

    def test_has_attr_read(self):
        self.assertTrue(hasattr(AbstractStorage, 'read'))

    def test_has_attr_update(self):
        self.assertTrue(hasattr(AbstractStorage, 'update'))

    def test_has_attr_delete(self):
        self.assertTrue(hasattr(AbstractStorage, 'delete'))

    def test_model_type_validation(self):

        class BadModel(object):
            pass

        class BadStorage(AbstractStorage):
            model_cls = BadModel

        self.assertRaises(AssertionError, BadStorage)

    def test_count(self):
        with patch('app.common.services.abstract_storage'
                   '.AbstractStorage._get_count') as mocked:
            mocked.return_value = 3
            result = MyStorage().count()
            mocked.assert_called_with()
            self.assertEqual(result, 3)

    def test_read_one(self):
        with patch('app.common.services.abstract_storage'
                   '.AbstractStorage._get_item') as mocked:
            model_item = mocked.return_value = MyModel(
                id=1, my_field=TEST_STR_1)
            result = MyStorage().read(id=1)
            mocked.assert_called_with(id=1)
            self.assertEqual(result, model_item)

    def test_read_many(self):
        with patch('app.common.services.abstract_storage'
                   '.AbstractStorage._get_list') as mocked:
            model_list = mocked.return_value = [
                MyModel(id=1, my_field=TEST_STR_1),
                MyModel(id=2, my_field=TEST_STR_2),
            ]
            result = MyStorage().read()
            mocked.assert_called_with(paging=None)
            self.assertEqual(result, model_list)

    def test_create(self):
        with patch('app.common.services.abstract_storage'
                   '.AbstractStorage._save') as mocked:
            model_item = mocked.return_value = MyModel(
                id=1, my_field=TEST_STR_1)
            result = MyStorage().create(my_field=TEST_STR_1)
            mocked.assert_called_with(
                MyModel(id=1, my_field=TEST_STR_1)
            )
            self.assertEqual(result, model_item)

    def test_update(self):
        with patch_multiple([
            'app.common.services.abstract_storage.AbstractStorage._get_item',
            'app.common.services.abstract_storage.AbstractStorage._save'
        ]) as (m_get_item, m_save):
            model_item = m_get_item.return_value = MyModel(
                id=1, my_field=TEST_STR_1)
            result = MyStorage().update(
                id=1, my_field=TEST_STR_2)
            m_get_item.assert_called_with(
                id=1)
            self.assertEqual(1, model_item.id)
            self.assertEqual(TEST_STR_2, model_item.my_field)
            m_save.assert_called_with(model_item)
            self.assertEqual(result, model_item)

    def test_delete(self):
        with patch('app.common.services.abstract_storage'
                   '.AbstractStorage._remove') as m_remove:
            result = MyStorage().delete(id=1)
            m_remove.assert_called_with(id=1)
            self.assertIsNone(result)

    def test_count_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._get_count()

    def test_get_item_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._get_item(id=1)

    def test_get_list_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._get_list()

    def test_save_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._save(model=MagicMock())

    def test_remove_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._remove(id=1)
