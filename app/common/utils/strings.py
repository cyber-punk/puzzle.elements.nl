# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def decapitalize(value):
    """
    Set string to lowercase
    and insert undreline symbol
    bettween chars that have different cases.
    For example:
        HelloWorld => hello_world

    :param value: capitalized string like 'CamelCase'
    :return: decapitalized string like 'camel_case'
    """
    low = value.lower()
    result = []
    for i in range(0, len(low)):
        if i and result[-1] != '_':
            if (low[i] != value[i] and low[i-1] == value[i-1] or
                    low[i].isdigit() != low[i-1].isdigit()):
                result.append('_')
        result.append(low[i])
    return ''.join(result)


def plural(value):
    """
    Set noun to plural form.
    For example:
        book => books
        box => boxes
        hero => heroes
        slash => slashes
        story => stories
        day => days
        wolf => wolves

    :param value: string in single form
    :return: string in plural form
    """
    if value[-1] in ('s', 'x', 'o') or value[-2:] in ('sh', 'ch'):
        return value + 'es'
    elif (value[-1] == 'y' and
          value[-2] not in ('a', 'e', 'i', 'o', 'u')):
        return value[:-1] + 'ies'
    elif value[-1] == 'f':
        return value[:-1] + 'ves'
    elif value[-2:] == 'fe':
        return value[:-2] + 'ves'
    else:
        return value + 's'
