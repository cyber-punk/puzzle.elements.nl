# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.utils import patch_multiple


class MyClass(object):

    @staticmethod
    def foo1(x, y):
        return x + y

    @staticmethod
    def foo2(x, y):
        return x - y

    @staticmethod
    def foo3(x, y):
        return x * y


class TestPatchMany(unittest.TestCase):

    def test_enter(self):
        with patch_multiple([
            'app.common.utils.tests.test_mocks.MyClass.foo1',
            'app.common.utils.tests.test_mocks.MyClass.foo2',
            'app.common.utils.tests.test_mocks.MyClass.foo3'
        ]) as (m_foo1, m_foo2, m_foo3):
            m_foo1.side_effect = lambda x, y: x + y * x
            m_foo2.side_effect = lambda x, y: (x + y) * x
            m_foo3.side_effect = lambda x, y: x * x + y * y
            self.assertEqual(9, MyClass.foo1(x=3, y=2))
            self.assertEqual(15, MyClass.foo2(x=3, y=2))
            self.assertEqual(13, MyClass.foo3(x=3, y=2))

    def test_return_value(self):
        with patch_multiple([
            'app.common.utils.tests.test_mocks.MyClass.foo1',
            'app.common.utils.tests.test_mocks.MyClass.foo2',
            'app.common.utils.tests.test_mocks.MyClass.foo3'
        ], return_value=42):
            self.assertEqual(42, MyClass.foo1(x=3, y=2))
            self.assertEqual(42, MyClass.foo2(x=3, y=2))
            self.assertEqual(42, MyClass.foo3(x=3, y=2))

    def test_side_effect(self):
        with patch_multiple([
            'app.common.utils.tests.test_mocks.MyClass.foo1',
            'app.common.utils.tests.test_mocks.MyClass.foo2',
            'app.common.utils.tests.test_mocks.MyClass.foo3'
        ], side_effect=lambda x, y: x * y * (x + 2 * y)):
            self.assertEqual(42, MyClass.foo1(x=3, y=2))
            self.assertEqual(42, MyClass.foo2(x=3, y=2))
            self.assertEqual(42, MyClass.foo3(x=3, y=2))

    def test_exit(self):
        with patch_multiple([
            'app.common.utils.tests.test_mocks.MyClass.foo1',
            'app.common.utils.tests.test_mocks.MyClass.foo2',
            'app.common.utils.tests.test_mocks.MyClass.foo3'
        ], return_value=42):
            pass
        self.assertEqual(5, MyClass.foo1(x=3, y=2))
        self.assertEqual(1, MyClass.foo2(x=3, y=2))
        self.assertEqual(6, MyClass.foo3(x=3, y=2))

    @unittest.expectedFailure
    def test_raises_on_exit(self):
        """
        We should not ignore any errors that happened in context.
        """
        with patch_multiple([
            'app.common.utils.tests.test_mocks.MyClass.foo1',
            'app.common.utils.tests.test_mocks.MyClass.foo2',
            'app.common.utils.tests.test_mocks.MyClass.foo3'
        ]):
            self.fail('Test should fail when exception raises '
                      'in patch context.')
