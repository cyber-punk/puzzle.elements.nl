# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.utils import classproperty


class Conejo(object):
    # pylint: disable=no-self-argument
    # pylint: disable=no-member

    @classproperty
    def saludo(cls):
        return 'Hola! Me llamo snr ' + cls.__name__


class Test(unittest.TestCase):

    def test_classproperty(self):
        self.assertEqual(Conejo.saludo, 'Hola! Me llamo snr Conejo')
