# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import mock


class MultiPatcher(object):

    def __init__(self):
        super(MultiPatcher, self).__init__()
        self._patchers = []

    def __call__(self, targets, *args, **kwargs):
        self._patchers = [mock.patch(target, *args, **kwargs)
                          for target in targets]
        return self

    def __enter__(self):
        return [patcher.__enter__() for patcher in self._patchers]

    def __exit__(self, *args):
        return all([patcher.__exit__(*args) for patcher in self._patchers])


patch_multiple = MultiPatcher()
