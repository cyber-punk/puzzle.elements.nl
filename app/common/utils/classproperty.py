# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class classproperty(object):
    """
    Example of usages:

    class Juan(object):

        @classproperty
        def saludo(cls):
            return 'Hola! Me llamo ' + cls.__name__

    >>> Juan.saludo
    >>> 'Hola! Me llamo Juan'
    """

    def __init__(self, getter):
        self.getter = getter

    def __get__(self, instance, owner):
        return self.getter(owner)
