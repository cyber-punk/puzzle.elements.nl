# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from mongoengine import Document, fields

from app.common.models.abstract_model import AbstractModel
from app.common.utils import classproperty, decapitalize, plural


class MongoModel(AbstractModel, Document):
    meta = {'abstract': True,
            'allow_inheritance': True}

    id = fields.UUIDField(primary_key=True, default=uuid.uuid4)

    @classmethod
    def _is_mongo_field(cls, attr_name):
        attr = getattr(cls, attr_name)
        return attr is not None and isinstance(attr, fields.BaseField)

    @classproperty
    def attr_names(cls):  # pylint: disable=no-self-argument
        result = super(MongoModel, cls).attr_names
        result = filter(cls._is_mongo_field, result)
        return result

    @classmethod
    def _get_collection_name(cls):
        return plural(decapitalize(cls.__name__))
