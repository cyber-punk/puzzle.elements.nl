# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.models import AbstractModel


class MyModel(AbstractModel):
    my_field = None


class Test(unittest.TestCase):

    def setUp(self):
        self.model = MyModel(id=1, my_field='Test')

    def test_init(self):
        self.assertEqual(self.model.id, 1)
        self.assertEqual(self.model.my_field, 'Test')

    def test_attr_names(self):
        self.assertListEqual(MyModel.attr_names, ['id', 'my_field'])

    def test_attrs(self):
        self.assertDictEqual(self.model.attrs, {'id': 1, 'my_field': 'Test'})

    def test_repr(self):
        self.assertEqual(str(self.model), '<MyModel: id=1, my_field=Test>')

    def test_equal(self):
        clone = MyModel(id=1, my_field='Test')
        self.assertEqual(self.model, clone)

    def test_not_equal_id(self):
        another = MyModel(id=2, my_field='Test')
        self.assertNotEqual(self.model, another)

    def test_not_equal_custom_field(self):
        another = MyModel(id=1, my_field='Hola!')
        self.assertNotEqual(self.model, another)

    def test_not_equal_types(self):
        class Another(MyModel):
            pass

        another = Another(id=1, my_field='Test')
        self.assertNotEqual(self.model, another)
