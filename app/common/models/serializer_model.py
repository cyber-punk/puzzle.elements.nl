# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow.fields import Field, missing_

from app.common.models.abstract_model import AbstractModel


class SerializerModel(AbstractModel):
    attr_names = []

    def __init__(self, serializer, data):
        """
        Set default values to model fields, then set attrs values
        :param serializer: instance of Abstract serializer or its child
        :param data: attrs dict
        """
        self.serializer = serializer
        if not self.serializer.partial:
            for attr_name, attr in self.serializer.fields.items():
                if isinstance(attr, Field):
                    self.__class__.attr_names.append(attr_name)
                    default_value = None
                    if callable(attr.default):
                        default_value = attr.default()
                    elif attr.default is not missing_:
                        default_value = attr.default
                    setattr(self, attr_name, default_value)
        super(SerializerModel, self).__init__(**data)

    @property
    def attrs(self):
        result = {}
        for field_name in self.serializer.fields.keys():
            result[field_name] = getattr(self, field_name)
        return result
