# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.utils import classproperty


class AbstractModel(object):
    id = None

    @classproperty
    def attr_names(cls):  # pylint: disable=no-self-argument
        result = []
        attrs = {}
        for base in cls.__bases__:  # pylint: disable=no-member
            if base is not cls:
                attrs.update(base.__dict__)
        attrs.update(cls.__dict__)
        if 'attr_names' in attrs:
            attrs.pop('attr_names')
        if 'attrs' in attrs:
            attrs.pop('attrs')
        for attr_name, attr in attrs.items():
            if not attr_name.startswith('_') and not callable(attr):
                result.append(attr_name)
        return result

    @property
    def attrs(self):
        return dict([
            (attr_name, getattr(self, attr_name))
            for attr_name in self.attr_names
        ])

    def __init__(self, **attrs):
        super(AbstractModel, self).__init__()
        for attr_name, attr_value in attrs.items():
            setattr(self, attr_name, attr_value)

    def __repr__(self):
        return '<{class_name}: {attrs}>'.format(
            class_name=self.__class__.__name__,
            attrs=', '.join(['{}={}'.format(name, self.attrs[name])
                             for name in self.attr_names])
        )

    def __str__(self):
        return self.__repr__()

    def __eq__(self, other):
        if type(self) != type(other):  # pylint: disable=unidiomatic-typecheck
            return False
        for attr_name in self.attrs:
            if getattr(self, attr_name) != getattr(other, attr_name):
                return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)
