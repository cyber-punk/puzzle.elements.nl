# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import uuid
import json
from copy import deepcopy

from django.http import HttpResponse, JsonResponse as DjangoJsonResponse
from django.conf import settings
from rest_framework import status
from rest_framework.views import APIView

from app.common.serializers import AbstractSerializer, FilterSerializer, \
    PagingSerializer
from app.common.serializers.exc import ValidationError
from app.common.services import AbstractStorage
from app.common.services.exc import DoesNotExist, AlreadyExists
from app.common.utils import decapitalize


logger = logging.getLogger(__name__)


class JsonResponse(DjangoJsonResponse):
    def __init__(self, *args, **kwargs):
        kwargs['safe'] = False
        super(JsonResponse, self).__init__(*args, **kwargs)


class InternalErrorResponce(JsonResponse):

    def __init__(self, error, **kwargs):
        error_id = uuid.uuid4()
        logger.error('%s : %s', error_id, error)
        data = {
            'error_id': error_id,
            'error': 'Internal server error. '
                     'Please, contact with system administrator.'
        }
        kwargs['status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
        super(InternalErrorResponce, self).__init__(data, **kwargs)


class AbstractResource(APIView):
    response_cls = JsonResponse
    serializer_cls = AbstractSerializer
    filter_serializer_cls = FilterSerializer
    paging_serializer_cls = PagingSerializer
    storage_cls = AbstractStorage

    def __init__(self, **kwargs):
        super(AbstractResource, self).__init__(**kwargs)

        self.Response = self.__class__.response_cls
        assert issubclass(self.Response, HttpResponse)

        self.serializer_cls = self.__class__.serializer_cls
        assert issubclass(self.serializer_cls, AbstractSerializer)
        self.serializer = self.serializer_cls()

        self.filter_serializer_cls = self.__class__.filter_serializer_cls
        assert issubclass(self.filter_serializer_cls, FilterSerializer)
        self.filter_serializer = self.filter_serializer_cls(self.serializer)

        self.paging_serializer_cls = self.__class__.paging_serializer_cls
        assert issubclass(self.paging_serializer_cls, PagingSerializer)
        self.paging_serializer = self.paging_serializer_cls()

        self.storage_cls = self.__class__.storage_cls
        assert issubclass(self.storage_cls, AbstractStorage)
        self.storage = self.storage_cls()

    @classmethod
    def get_endpoint(cls):
        return decapitalize(cls.__name__)

    @staticmethod
    def _get_request_url_data(request, **kwargs):
        data = deepcopy(kwargs)
        data.update(request.GET.dict())
        return data

    @staticmethod
    def _get_request_body_data(request, **kwargs):
        data = json.loads(request.body)
        if kwargs and isinstance(data, dict):
            data.update(kwargs)
        elif kwargs and isinstance(data, list):
            for item in data:
                item.update(kwargs)
        return data

    def get(self, request, **kwargs):
        try:
            data = self._get_request_url_data(request, **kwargs)
            filter_data = self.filter_serializer.load_data(data)
            if filter_data.get('id') is not None:
                storage_data = self.storage.read(**filter_data)
                result = self.serializer.dump_data(storage_data)
            else:
                paging = self.paging_serializer.load_data(data)
                paging.total = self.storage.count(**filter_data)
                storage_data = self.storage.read(paging=paging, **filter_data)
                paging.count = len(storage_data)
                paging_data = self.paging_serializer.dump_data(paging)
                result_data = self.serializer.dump_data(storage_data)
                result = dict(paging=paging_data, data=result_data)
            return self.Response(result)
        except ValidationError as e:
            return self.Response(e.errors,
                                 status=status.HTTP_400_BAD_REQUEST)
        except DoesNotExist as e:
            return self.Response({'error': e.description},
                                 status=status.HTTP_404_NOT_FOUND)
        except Exception as e:  # pylint: disable=broad-except
            if settings.DEBUG:
                raise
            return InternalErrorResponce(e)

    def post(self, request, **kwargs):
        try:
            data = self._get_request_body_data(request, **kwargs)
            validated_data = self.serializer.load_data(data, partial=False)
            if isinstance(validated_data, list):
                storage_data = []
                for item in validated_data:
                    storage_data.append(
                        self.storage.create(**item.attrs)
                    )
            else:
                storage_data = self.storage.create(**validated_data.attrs)
            # Classic REST should return just id in headers without body,
            # but we return created model data for more usable.
            result = self.serializer.dump_data(storage_data)
            return self.Response(result,
                                 status=status.HTTP_201_CREATED)
        except ValidationError as e:
            return self.Response(e.errors,
                                 status=status.HTTP_400_BAD_REQUEST)
        except DoesNotExist as e:
            return self.Response({'error': e.description},
                                 status=status.HTTP_404_NOT_FOUND)
        except AlreadyExists as e:
            return self.Response({'error': e.description},
                                 status=status.HTTP_409_CONFLICT)
        except Exception as e:  # pylint: disable=broad-except
            if settings.DEBUG:
                raise
            return InternalErrorResponce(e)

    def patch(self, request, **kwargs):
        try:
            data = self._get_request_body_data(request, **kwargs)
            validated_data = self.serializer.load_data(data, partial=True)
            if isinstance(validated_data, list):
                storage_data = []
                for item in validated_data:
                    storage_data.append(
                        self.storage.update(**item.attrs)
                    )
            else:
                storage_data = self.storage.update(**validated_data.attrs)
            # Classic REST should return 204 no content, without body,
            # but we return updated model data for more usable.
            result = self.serializer.dump_data(storage_data)
            return self.Response(result)
        except ValidationError as e:
            return self.Response(e.errors,
                                 status=status.HTTP_400_BAD_REQUEST)
        except DoesNotExist as e:
            return self.Response({'error': e.description},
                                 status=status.HTTP_404_NOT_FOUND)
        except AlreadyExists as e:
            return self.Response({'error': e.description},
                                 status=status.HTTP_409_CONFLICT)
        except Exception as e:  # pylint: disable=broad-except
            if settings.DEBUG:
                raise
            return InternalErrorResponce(e)

    def delete(self, request, **kwargs):
        try:
            data = self._get_request_url_data(request, **kwargs)
            filter_data = self.filter_serializer.load_data(data)
            if isinstance(filter_data, list):
                for item in filter_data:
                    self.storage.delete(**item)
            else:
                self.storage.delete(**filter_data)
            return self.Response({}, status=status.HTTP_204_NO_CONTENT)
        except ValidationError as e:
            return self.Response(e.errors,
                                 status=status.HTTP_400_BAD_REQUEST)
        except DoesNotExist as e:
            return self.Response({'error': e.description},
                                 status=status.HTTP_404_NOT_FOUND)
        except Exception as e:  # pylint: disable=broad-except
            if settings.DEBUG:
                raise
            return InternalErrorResponce(e)
