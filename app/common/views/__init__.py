# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.views.abstract_resource import AbstractResource


__all__ = [
    'AbstractResource',
]
