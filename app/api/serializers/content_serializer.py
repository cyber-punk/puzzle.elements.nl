# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import fields, pre_dump
from marshmallow.validate import Length
from django.conf import settings
from django.urls import reverse

from app.common.serializers import AbstractSerializer


class ContentSerializer(AbstractSerializer):
    title = fields.String(validate=Length(max=settings.MAX_TITLE_LENGTH))
    description = fields.String(
        validate=Length(max=settings.MAX_DESCRIPTION_LENGTH))
    image_id = fields.UUID()
    image_link = fields.String(required=False, dump_only=True)

    @pre_dump
    def get_image_link(self, obj):  # pylint: disable=no-self-use
        obj.image_link = None
        if obj.image_id is not None:
            obj.image_link = reverse('image_resource',
                                     kwargs={'id': obj.image_id})
        return obj
