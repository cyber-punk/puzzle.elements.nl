# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import fields, UnmarshalResult
from marshmallow.validate import Range, OneOf
from django.conf import settings

from app.common.serializers import AbstractSerializer


class ImageSerializer(AbstractSerializer):
    image_format = fields.String(
        validate=OneOf(settings.ALLOWED_IMAGE_FORMATS))
    width = fields.Integer(validate=Range(min=1, max=settings.MAX_IMAGE_WIDTH))
    height = fields.Integer(
        validate=Range(min=1, max=settings.MAX_IMAGE_HEIGHT))

    def dump(self, image, *args, **kwargs):  # pylint: disable=unused-argument
        return UnmarshalResult(errors=[], data=image)
