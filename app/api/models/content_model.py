# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mongoengine import fields
from django.conf import settings

from app.common.models import MongoModel


class Content(MongoModel):
    title = fields.StringField(max_length=settings.MAX_TITLE_LENGTH)
    description = fields.StringField(
        max_length=settings.MAX_DESCRIPTION_LENGTH)
    image_id = fields.UUIDField()
