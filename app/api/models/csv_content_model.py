# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.models import AbstractModel


class CSVContent(AbstractModel):
    title = None
    description = None
    image = None
