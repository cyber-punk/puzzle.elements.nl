# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mongoengine import fields
from django.conf import settings

from app.common.models import MongoModel


class Image(MongoModel):
    """
    Can be existed many images with the same image_id
    but different sizes and/or content-types.
    origin_id is the origin content's meaning.
    """
    origin_id = fields.UUIDField()
    image_format = fields.StringField(max_length=32)
    width = fields.IntField(min_value=1, max_value=settings.MAX_IMAGE_WIDTH)
    height = fields.IntField(min_value=1, max_value=settings.MAX_IMAGE_HEIGHT)
    image_data = fields.ImageField(
        size=(settings.MAX_IMAGE_WIDTH, settings.MAX_IMAGE_HEIGHT, True),
        thumbnail_size=settings.THUMBNAIL_IMAGE_SIZE)
