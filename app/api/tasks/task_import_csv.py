# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

from datetime import timedelta

from celery.task import periodic_task
from celery.utils.log import get_task_logger
from django.conf import settings

from app.api.services import CSVImporter


@periodic_task(name='app.api.tasks.task_import_csv.import_csv',
               run_every=timedelta(minutes=15))
def import_csv():
    importer = CSVImporter(logger=get_task_logger(__name__))
    return importer.update_data(url=settings.CSV_DATA_URL)
