# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.conf import settings
from django.views.decorators.cache import cache_page
from rest_framework import routers


from app.api.views import ContentResource, ImageResource


router = routers.SimpleRouter()


router.urls.append(
    url('contents$',
        cache_page(settings.CONTENT_CACHE_TTL)(
            ContentResource.as_view()),
        name=ContentResource.get_endpoint())
)
router.urls.append(
    url('contents/(?P<id>[^/]+)$',
        cache_page(settings.CONTENT_CACHE_TTL)(
            ContentResource.as_view()),
        name=ContentResource.get_endpoint())
)
router.urls.append(
    url('images/(?P<id>[^/]+)$',
        cache_page(settings.IMAGE_CACHE_TTL)(
            ImageResource.as_view()),
        name=ImageResource.get_endpoint())
)
