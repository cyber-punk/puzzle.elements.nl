# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function

import sys
import logging
from optparse import make_option

from django.core.management import BaseCommand
from django.conf import settings

from app.api.services import CSVImporter


class Command(BaseCommand):
    help = 'Import data from network csv file'

    option_list = [
        make_option(
            '--url',
            default=settings.CSV_DATA_URL,
            type='str',
            dest='csv_url',
            help='URL to the network based csv file with data.'
        ),
    ]

    def handle(self, *args, **options):
        url = options.get('url', settings.CSV_DATA_URL)
        logger = logging.getLogger()
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(logging.DEBUG if settings.DEBUG else logging.INFO)
        importer = CSVImporter(logger=logger)
        importer.update_data(url)
