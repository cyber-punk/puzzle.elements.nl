# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import uuid
import hashlib
from io import BytesIO

import requests
from PIL import Image as PILImage, ImageOps
from django.conf import settings

from app.api.models import Content, Image
from app.api.services.csv_content_storage import CSVContentStorage


class CSVImporter(object):

    def __init__(self, logger=None):
        super(CSVImporter, self).__init__()
        self.logger = logger or logging.getLogger(__name__)

    def update_data(self, url):
        r = requests.get(url)
        csv_loader = CSVContentStorage(r.iter_lines(), logger=self.logger)
        i = -1
        for item in csv_loader.read():
            i += 1
            try:
                content = Content(title=item.title,
                                  description=item.description)
                if item.image is not None:
                    content.image_id = self.store_image(url=item.image)
                content.id = self._calculate_content_id(
                    item.title, item.description, item.image)
                content.save()
                self.logger.info('Row: {}. Success. {}\r\n'.format(i, content))
            except Exception as e:  # pylint: disable=broad-except
                self.logger.error('Row: {} Error: {}\r\n'.format(i, str(e)))
        return i

    def store_image(self, url):
        try:
            image = self._get_image(url)
            image.save()
            return image.id
        except Exception as e:  # pylint: disable=broad-except
            self.logger.error('Image url: {} Error: {}'.format(url, str(e)))

    def _get_image(self, url):
        r = requests.get(url)

        content_type = r.headers['content-type']
        self.logger.debug('content-type: {}'.format(content_type))
        if content_type not in settings.ALLOWED_IMAGE_CONTENT_TYPES:
            raise ValueError(
                'Not supported content type: {}'.format(content_type))

        img = PILImage.open(BytesIO(r.content))
        image_format = img.format.lower()
        self.logger.debug('Image format: {}'.format(image_format))
        if image_format not in settings.ALLOWED_IMAGE_FORMATS:
            raise ValueError(
                'Not supported image format: {}'.format(image_format))

        origin_id = self._calculate_image_id(r.content)

        width, height = img.size
        self.logger.debug('width: {}, height: {}'.format(width, height))
        if (width > settings.MAX_IMAGE_WIDTH or
                height > settings.MAX_IMAGE_HEIGHT):
            img = ImageOps.fit(
                image=img,
                size=(settings.MAX_IMAGE_WIDTH, settings.MAX_IMAGE_HEIGHT),
                method=PILImage.ANTIALIAS)
            width, height = img.size
        self.logger.debug('width: {}, height: {}'.format(width, height))

        output = BytesIO()
        img.save(output, format='PNG')
        image_data = output

        return Image(id=origin_id, origin_id=origin_id, image_format='png',
                     width=width, height=height, image_data=image_data)

    @staticmethod
    def _calculate_content_id(title, description, image_url):
        s = '{}_{}_{}'.format(title, description, image_url)
        return uuid.UUID(bytes=hashlib.sha1(s).digest()[:16], version=4)

    @staticmethod
    def _calculate_image_id(data):
        return uuid.UUID(bytes=hashlib.sha1(data).digest()[:16], version=4)
