# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.api.models import Content
from app.common.services import MongoStorage


class ContentStorage(MongoStorage):
    model_cls = Content
