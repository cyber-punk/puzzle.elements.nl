# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from io import BytesIO

from PIL import Image as PILImage, ImageOps

from app.api.models import Image
from app.common.services import MongoStorage
from app.common.services.exc import DoesNotExist


class ImageStorage(MongoStorage):
    model_cls = Image

    def _get_item(self, **attrs):
        origin_id = attrs.pop('id')
        try:
            return super(ImageStorage, self)._get_item(
                origin_id=origin_id, **attrs)
        except DoesNotExist:
            if not attrs:
                raise
            origin = super(ImageStorage, self)._get_item(origin_id=origin_id)
            modified = self._modify_image(origin, **attrs)
            modified.save()
            return modified

    @staticmethod
    def _modify_image(origin, **attrs):
        width = attrs.get('width', origin.width)
        height = attrs.get('height', origin.height)
        image_format = attrs.get('image_format', origin.image_format)
        img = PILImage.open(BytesIO(origin.image_data.read()))
        img = ImageOps.fit(image=img, size=(width, height),
                           method=PILImage.ANTIALIAS)
        width, height = img.size
        image_data = BytesIO()
        img.save(image_data, format=image_format)
        return Image(origin_id=origin.origin_id,
                     image_format=image_format,
                     width=width, height=height,
                     image_data=image_data)
