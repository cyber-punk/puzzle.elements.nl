# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest
import os

from app.api.services import CSVContentStorage
from app.api.models import CSVContent
from app.common.services import CSVStorage
from app.common.services.exc import ReadOnlyError
from app.common.models import AbstractModel


class Paging(AbstractModel):
    attr_names = ['offset', 'limit']
    offset = None
    limit = None


class Test(unittest.TestCase):

    def setUp(self):
        self.file_name = os.path.abspath(
            os.path.join(os.path.dirname(__file__), 'test.csv')
        )
        with open(self.file_name, 'r') as f:
            self.items = CSVContentStorage(f).read()

    def test_count(self):
        with open(self.file_name, 'r') as f:
            count = CSVContentStorage(f).count()
        self.assertEqual(count, 24)

    def test_no_model_count(self):
        with open(self.file_name, 'r') as f:
            count = CSVStorage(f).count()
        self.assertEqual(count, 25)

    def test_read_normal(self):
        self.assertEqual(
            self.items[0],
            CSVContent(
                title='Item 1',
                description='Description 1',
                image='http://farm4.staticflickr.com/3764/'
                      '10438039923_2ef6f68348_c.jpg'
            ))

    def test_read_multi_lines(self):
        self.assertEqual(
            self.items[9],
            CSVContent(
                title='Item 10 Extra line 1 Extra line 2 Extra line 3',
                description='Description 10',
                image='https://www.google.nl'
            ))

    def test_read_multi_quotes(self):
        self.assertEqual(
            self.items[13],
            CSVContent(
                title='Item 14, "extra data"',
                description='Description 14',
                image='http://www.rubberdragon.com/website-design/44/content/'
                      'just-in-time-stock.png?1290605562'
            ))

    def test_read_not_all(self):
        self.assertEqual(
            self.items[3],
            CSVContent(
                title='Item 4',
                description='Description 4, <i>extra info</i>'
            ))

    def test_read_quoted_comma(self):
        self.assertEqual(
            self.items[18],
            CSVContent(
                title='Item 19',
                description='Description 19',
                image='https://www.google.nl/url?sa=i&rct=j&q=&esrc=s'
                      '&source=images&cd=&cad=rja&docid=vp47OjPgb8jYjM'
                      '&tbnid=mB4KC2XedUiDgM:&ved=0CAUQjRw'
                      '&url=http%3A%2F%2Fwww.naturestocklibrary.com%2F'
                      '&ei=O8pjUs3_MIbI0QXUnIDIAw&bvm=bv.54934254,d.d2k'
                      '&psig=AFQjCNHp_WiQTk5MfZ8JliMxj6JYTc206w'
                      '&ust=1382357938513870'
            ))

    def test_read_filter(self):
        with open(self.file_name, 'r') as f:
            items = CSVContentStorage(f).read(title='Item 11')
        self.assertEqual(len(items), 1)
        self.assertEqual(
            items[0],
            CSVContent(
                title='Item 11',
                description='Description 11'
            ))

    def test_read_paging(self):
        with open(self.file_name, 'r') as f:
            items = CSVContentStorage(f).read(
                paging=Paging(offset=10, limit=10))
        self.assertEqual(len(items), 10)
        self.assertEqual(
            items[0],
            CSVContent(
                title='Item 12',
                description='Description 12',
                image='http://comps.canstockphoto.com/'
                      'can-stock-photo_csp9177473.jpg'
            ))
        self.assertEqual(
            items[-1],
            CSVContent(
                title='Item 21',
                description='Description 21',
                image='https://www.google.com'
            ))

    def test_read_item_is_not_supported(self):
        with open(self.file_name, 'r') as f:
            item = CSVContentStorage(f).read(id=1)
        self.assertIsNone(item)

    def test_create_is_not_supported(self):
        with self.assertRaises(ReadOnlyError):
            with open(self.file_name, 'r') as f:
                CSVContentStorage(f).create(title='Item 100')

    def test_update_is_not_supported(self):
        with self.assertRaises(ReadOnlyError):
            with open(self.file_name, 'r') as f:
                CSVContentStorage(f).update(title='Item 1')

    def test_delete_is_not_supported(self):
        with self.assertRaises(ReadOnlyError):
            with open(self.file_name, 'r') as f:
                CSVContentStorage(f).delete(title='Item 1')
