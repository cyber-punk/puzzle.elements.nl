# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.api.models import CSVContent
from app.common.services import CSVStorage


class CSVContentStorage(CSVStorage):
    model_cls = CSVContent
