# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, JsonResponse

from app.api.services import ImageStorage
from app.api.serializers import ImageSerializer
from app.api.models import Image
from app.common.views import AbstractResource


class ImageResponse(JsonResponse):
    """
    Returns image content when content is Image model instance.
    In other cases it returns standart json response.
    """

    def __init__(self, content, *args, **kwargs):
        if isinstance(content, Image):
            kwargs['content_type'] = 'image/{}'.format(content.image_format)
            HttpResponse.__init__(  # pylint: disable=non-parent-init-called
                self, content=content.image_data.read(), *args, **kwargs)
        else:
            kwargs['safe'] = False
            super(ImageResponse, self).__init__(content, *args, **kwargs)


class ImageResource(AbstractResource):
    """
    Supported links:
        /images/<uid>?width=x&height=y
    Query parameters are not required.
    """
    http_method_names = ['get']
    response_cls = ImageResponse
    storage_cls = ImageStorage
    serializer_cls = ImageSerializer
