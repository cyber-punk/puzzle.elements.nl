# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.api.views.content_resource import ContentResource
from app.api.views.image_resource import ImageResource

__all__ = [
    'ContentResource',
    'ImageResource',
]
