# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.api.services import ContentStorage
from app.api.serializers import ContentSerializer
from app.common.views import AbstractResource


class ContentResource(AbstractResource):
    # http_method_names = ['get']
    storage_cls = ContentStorage
    serializer_cls = ContentSerializer
