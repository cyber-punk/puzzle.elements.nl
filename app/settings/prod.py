# -*- coding: utf-8 -*-
# pylint: disable=wildcard-import
# pylint: disable=unused-wildcard-import
from __future__ import unicode_literals

from app.settings.qa import *


# shodan: port:27017 country:US MongoDB Server Information
MONGODB_DATABASES['default']['name'] = 'puzzle_elements_nl_prod'
MONGODB_DATABASES['default']['host'] = '52.0.126.252:27017'
# MONGODB_DATABASES['default']['host'] = (
#     'mongodb://{}/?replicaSet={}'.format(', '.join([
#         '52.0.126.252:27017',
#         '54.205.5.160:27017',
#         '52.21.107.21:27017',
#     ]), 'puzzle_elements_nl_prod')
# )

# shodan: port:6379 country:US uptime_in_seconds
CACHES['default']['LOCATION'] = [
    'redis://52.72.21.135:6379/1',
    'redis://52.78.191.68:6379/2',
    'redis://52.32.196.82:6379/3',
]

# shodan: port:5672 country:US RabbitMQ
BROKER_URL = 'amqp://guest:guest@54.224.109.40:5672//'
CELERY_RESULT_BACKEND = 'redis://54.85.75.83:6379/1'
