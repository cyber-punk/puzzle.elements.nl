# -*- coding: utf-8 -*-
# pylint: disable=wildcard-import
# pylint: disable=unused-wildcard-import
from __future__ import unicode_literals

from app.settings.dev import *


SECRET_KEY = os.getenv('SECRET_KEY')

DEBUG = False
TEST = False

MONGODB_DATABASES['default']['name'] = 'puzzle_elements_nl_qa'
