# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from datetime import timedelta

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv(
    'SECRET_KEY',
    'l2v@-(c(p$j1@1b10mhlu5n58+^_+8zaffz22-!5^lv$)e6d8+'
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEST = False

ALLOWED_HOSTS = []


# Internationalization

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Databases

DATABASES = {
    'default': {'ENGINE': 'django.db.backends.dummy'},
}

MONGODB_DATABASES = {
    'default': {
        'name': 'puzzle_elements_nl_dev',
        'host': 'mongodb://127.0.0.1:27017',
        'tz_aware': USE_TZ,
    },
}

# Redis cache

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/0',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.ShardClient',
            'COMPRESSOR': 'django_redis.compressors.zlib.ZlibCompressor',
            'IGNORE_EXCEPTIONS': True,
        },
    },
}

# Celery

BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'

# Application definition

# django apps
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# third party apps
INSTALLED_APPS += [
    'rest_framework',
    'djcelery',
]

# my apps
INSTALLED_APPS += [
    'app.api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'

# rest framework
REST_FRAMEWORK = {
}

# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'


# Images

MAX_IMAGE_WIDTH = 1024
MAX_IMAGE_HEIGHT = 768
THUMBNAIL_IMAGE_SIZE = (320, 320)
ALLOWED_IMAGE_CONTENT_TYPES = [
    'image/png',
    'image/jpg',
    'image/jpeg',
    'image/gif',
]
ALLOWED_IMAGE_FORMATS = [
    'png',
    'jpeg',
    'gif',
]
IMAGE_CACHE_TTL = 3600


# CSV data

CSV_DATA_URL = ('https://docs.google.com/spreadsheet/ccc'
                '?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE'
                '&single=true&gid=0&output=csv')

MAX_TITLE_LENGTH = 256
MAX_DESCRIPTION_LENGTH = 2048

CONTENT_CACHE_TTL = 30
