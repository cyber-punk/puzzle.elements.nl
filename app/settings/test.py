# -*- coding: utf-8 -*-
# pylint: disable=wildcard-import
# pylint: disable=unused-wildcard-import
from __future__ import unicode_literals

from app.settings.dev import *


DEBUG = False
TEST = True

MONGODB_DATABASES['default']['name'] = 'puzzle_elements_nl_test'
MONGODB_DATABASES['default']['host'] = 'mongodb://127.0.0.1:27017'

CACHES['default']['LOCATION'] = 'redis://127.0.0.1:6379/0'

CONTENT_CACHE_TTL = 0
IMAGE_CACHE_TTL = 0

BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
